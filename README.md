# Logger

Simple plugin script which saves all "logger" logs to txt file. 

# USAGE
### 1. Add file to project (anywhere you want)

### 2. Import module in file where you want to logger something 
```
const logger = require('./logger');
```

### 3. Initialize logger
```
logger.initialize();
```

Method takes two arguments:
- [string] path to save log files ex "./logger_logs"
- [string] name of logger logs file ex "logger_log"

If You want to specify only name then set first parameter as empty string

```
logger.initialize('', 'logfile_name');
```

### 4. Use logger in code

```
const test = {
	amount: 5
}

logger.log(test);
```

# Additional information

1. Logger can log also objects and arrays (and arrays of objects, objects which contains arrays etc.) 

    IF YOU LOG OBJECT OR ARRAY YOU CAN PASS SECONDS PARAMETER WHICH MUST BE STRING. (example bellow)
    ```
    const user_profile = {
      name: "John"
      level: 2
    }

    logger.log(user_profile, "User profile object")
    ```

2. Logger will create catalogue in `logger.js` file directory

3. With every server rebuild logger will create new logger logs file but it will also save previous logger logs file in the same directory under the name with pattern `logs_up_to_` + `new Date().getTime()`

4. IMPORTANT - logger method takes **ONLY** one argument which can be `number`, `string` or two arguments where one is `object` or `array` and second can be only `string`. 
(logger method can be also used as extension to ex. console.log method but when pass only one parameter)

5. To get last logs from server you can specify endpoint for that (example bellow)

```
// ===== Endpoint to get logs written in file.txt =====
app.get('/getlog', (req, res, next) => {
  fs.readFile(logger.getPath(), function (err, buff) {
    res.download(logger.getPath());
  });
})
```

**ENJOY :)**