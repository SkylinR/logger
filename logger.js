/* LOGGER PACKAGE MADE BY SKYLNR FOR NodeJS PROJECTS TO CREATE LOG FILE INSIDE PROJECT
TO USE IT JUST ADD THOSE TWO LINE TO SERVER MAIN JS FILE:
const logger = require('./logger');
logger.initialize(); // Here You can specify path and file name as two parameters of function
TO ADD SOMETHING TO LOG JUST USE METHOD "logger.log". Can use second parameter to specify name of file*/

var fs = require('fs');
var filePath = "";
var isInitialized = false;
const indent = `  `;

initialize = (path = "./logger_logs", name = "logger_log") => {
    const logText = `================= LOGS FROM LOGGER ==================\n`;

    if (path != "" || path != "./") {
        path += "/";
    }

	if(name == "") {
		name = "logger_log";
	}
	
	name += ".txt"
    filePath = path + name;

    if (!fs.existsSync(path)) {
        try {
            fs.mkdirSync(path);
        }
        catch (e) {
            mkdirpath(path.dirname(path));
            mkdirpath(path);
        }
    }

    if (fs.existsSync(filePath)) {
        fs.copyFile(filePath, path + "logs_up_to_" + new Date().getTime().toString() + ".txt", (err) => {
            if (err) throw err;
            fs.writeFile(path + name, logText, function (err) {
                if (err) throw err;
                console.log("\x1b[35m", "Logger initialized...", "\x1b[0m");
                isInitialized = true;
            });
        });
    }
    else {
        fs.writeFile(path + name, logText, function (err) {
            if (err) throw err;
            console.log("\x1b[35m", "Logger initialized...", "\x1b[0m");
            isInitialized = true;
        });
    }

};

log = (log, varName = "") => {

    checkIfWasInitialized();

    const currentTimestamp = new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().split('.')[0].replace('T', ' ');
    var logText = `\n${currentTimestamp} - `;

    if (isObject(log)) {
        logText += `${varName}\n ======= Object is logged under this line ========\n`;
        logText += `${objectToString(log)}`;
        logText += `\n========= object end ========\n`;
    }
    else if (isArray(log)) {
        logText += `${varName}\n ======= Array is logged under this line ========\n`;
        logText += `${arrayToString(log)}`;
        logText += `\n========= array end ========\n`;
    }
    else {
        if (varName != "") {
            varName += `\n `;
        }
        logText += `${varName}${log}\n`;
    }
    appendStingToFile(logText);
};

objectToString = (object, nested = 0, objectName = 'obj') => {
    var objectString = `${indent.repeat(nested)}${objectName}:{\n`;
    var objectKeys = Object.keys(object);

    for (var objectKey of objectKeys) {

        if (isObject(object[objectKey])) {
            objectString += `${objectToString(object[objectKey], nested + 1)}`;
        }
        else if (isArray(object[objectKey])) {
            objectString += `${arrayToString(object[objectKey], nested + 1)},\n`;
        }
        else {
            objectString += `${indent.repeat(nested + 1)}${objectKey}: ${object[objectKey]},\n`;
        }
    }

    // objectString = objectString.substring(0, objectString.length -1);
    return objectString + `${indent.repeat(nested)}},\n`;
};

arrayToString = (arr, nested = 0, arrayName = 'arr') => {
    var objectString = `${indent.repeat(nested)}${arrayName}:[`;

    for (var arrElement of arr) {

        if (isObject(arrElement)) {
            objectString += `\n${indent.repeat(nested + 1)}{\n${objectToString(arrElement, nested + 2)}${indent.repeat(nested + 1)}},`;
        }
        else if (isArray(arrElement)) {
            objectString += `\n${arrayToString(arrElement, nested + 1)},`;
        }
        else {
            objectString += `\n${indent.repeat(nested + 1)}${arrElement},`
        }
    }

    // objectString = objectString.substring(0, objectString.length -1);
    return objectString + `\n${indent.repeat(nested)}]`;
};

isObject = function (a) {
    return (!!a) && (a.constructor === Object);
};

isArray = function (a) {
    return (!!a) && (a.constructor === Array);
};

getPath = () => {
    return filePath;
}

checkIfWasInitialized = () => {
    if (!isInitialized) {
        initialize("", "logger_log.txt")
        console.error("LOGGER WAS NOT INITALIZED. FILE WAS CREATED IN THE logger.js FILE DIRECTION.");
    };
}

appendStingToFile = (logString) => {
    fs.appendFile(filePath, logString, function (err) {
        if (err) throw err;
    });
}

module.exports = {
    initialize: initialize,
    log: log,
    getPath: getPath
};